<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'Auth\LoginController@login_user')->name('login');
Route::get('/loginUser', 'Auth\LoginController@loginUser')->name('loginUser');
Route::get('/registerUser', 'Auth\LoginController@registerUser')->name('registerUser');
Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');

Route::group(['middleware' => ['check-permission']], function () {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/role/delete/{id}', 'RoleController@delete')->name('role.delete');
    Route::resource('/role', 'RoleController');
    Route::resource('/user', 'UserController');
    Route::get('/server/delete/{id}', 'ServerController@delete')->name('server.delete');
    Route::resource('/server', 'ServerController');

});

Route::get('/generate_password', 'GeneratePasswordController@generate_password')->name('generate.password');
Route::get('/check_server_creds/{email}/{ip_addr}', 'GeneratePasswordController@check_server_creds')->name('check.server_creds');
Route::get('/check_otp/{otp}/{id}', 'GeneratePasswordController@check_otp')->name('check.otp');

