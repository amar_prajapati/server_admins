## Application installation steps

git clone https://amar_prajapati@bitbucket.org/amar_prajapati/server_admins.git
1. Upload .env file and first create database and then edit database name, username and password in .env file.
2. run command "composer install"
3. run command "php artisan migrate"
4. run command "php artisan db:seed"

