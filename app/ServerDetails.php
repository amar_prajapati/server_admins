<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class ServerDetails extends Model
{
    protected $table = 'server_details';

    protected $fillable = [
        'email_id',
        'ip_addr',
        'password'
    ];
    public static function validate($request){
        $validatedata = Validator::make($request->input(), [
            'email_id' => 'required',
            'ip_addr' => 'required',
            'password' => 'required'
        ]);

        return $validatedata;
    }
}
