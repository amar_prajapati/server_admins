<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::with(['roles'])->where('id', Auth::user()->id)->first();
//        dump($user);
        $roles = array_get($user, 'roles')->first();
//         dd($roles);

        if(isset($roles->redirect_to))
        {
            $role_name = $roles->name;
            return redirect()->route($roles->redirect_to);
        }

        // redirect()->url();
//        return view('welcome');

         abort(404);
    }
}
