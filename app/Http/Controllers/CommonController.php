<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryLocation;
use Config;
use File;
use Storage;
use App\Mail\SendMailable;
use App\Mail\POApproval;
use Illuminate\Support\Facades\Mail;
use App\PRnumber;
use App\PoNumber;
use Session;

class CommonController extends Controller
{
    public function file_upload($folder_name, $file, $file_name){
        return Storage::disk('local')->putFileAs($folder_name, $file, $file_name);
    }

    public function file_delete($path){
        return Storage::disk('local')->delete($path);
    }

    /**
     * Save PDF
     */

    public function savePDF($id){


        $ids = explode('_',$id);
        
        if($ids[1] == 'po'){
            $title = 'Purchase Order';
            $pr_number = PoNumber::with(['get_company_name','get_deliverylocation_name','get_vendor_name','delivery_terms', 'gst_no', 'approved_user', 'pr_nos' => function($q){
                $q->with(['users'])->first();
            }])->where('po_numbers.id',$ids[0])->first();
            // dd($pr_number->approval_code);

            if($pr_number->approval_code == null){

                $code = substr($pr_number->pr_nos->pr_no, 3).date("dmyhis");
                Session::put('code', $code);

            }else{
                
                if(Session::get('code_request')){
                    if($pr_number->approval_code != Session::get('code_request')){
                        $msg_arr = array('msg' => 'Enter the correct code.');
                        return $msg_arr;
                    }else{
                        
                        $arr = array(
                            'approval_code' => 1
                        );
                        Session::forget('code_request');
                        $po_number = PoNumber::where('id', $pr_number->id)->update($arr);
                        $pr_number->approval_code = 1;
                    }
                }
            }
            
            if(Session::has('code')){
                Session::put('to_email', Config::get('commonConfig.code_email_id'));
            }else{
                Session::put('to_email', $pr_number->pr_nos->users->email);
            }
            
           /* $pr_number = \DB::table('po_numbers as prn')
                ->leftJoin('company as c','prn.company_id','=','c.id')
                ->leftJoin('delivery_location as dl','prn.delivery_location','=','dl.id')
                ->leftJoin('vendor as v','prn.vendor_supplier_name','=','v.id')

                ->leftJoin('terms_of_deliveries as deli','prn.terms_of_delivery','=','deli.id')
                ->where('prn.id',$ids[0])
                ->first([
                    'prn.*',
                    'c.company_name as company',
                    'c.gstin_uin as company_gstin_uin',
                    'c.pan as company_pan',
                    'c.address as company_address',
                    'dl.location as delivery_location',
                    'v.vendor_name as vendor_supplier_name',
                    'v.gstin_uin',
                    'v.address',
                    'deli.term_of_delivery'
                ]);*/

            if(!$pr_number){
                throw new \Exception('Pr No does not exist');
            }

            $pr_items=\DB::table('p_ritems')
                ->where('pr_no_id', $pr_number->pr_nos->id)
                ->get();

            $note = '';
        }

        if($ids[1] == 'pr'){
            $title = 'Purchase Request';
            $pr_number = PRnumber::with('get_company_name','get_deliverylocation_name','get_vendor_name','delivery_terms', 'gst_no', 'approved_user')
                                    ->where('p_rnumbers.id',$ids[0])->first();
           /* $pr_number = \DB::table('p_rnumbers as prn')
                ->leftJoin('company as c','prn.company_id','=','c.id')
                ->leftJoin('delivery_location as dl','prn.delivery_location','=','dl.id')
                ->leftJoin('vendor as v','prn.vendor_supplier_name','=','v.id')
                ->leftJoin('terms_of_deliveries as deli','prn.terms_of_delivery','=','deli.id')
                ->where('prn.id',$ids[0])
                ->first([
                    'prn.*',
                    'c.company_name as company',
                    'c.gstin_uin as company_gstin_uin',
                    'c.pan as company_pan',
                    'c.address as company_address',
                    'dl.location as delivery_location',
                    'v.vendor_name as vendor_supplier_name',
                    'v.gstin_uin',
                    'v.address',
                    'deli.term_of_delivery'
                ]);*/

            if(!$pr_number){
                throw new \Exception('Pr No does not exist');
            }

            $pr_items=\DB::table('p_ritems')
                ->where('pr_no_id',$ids[0])
                ->get();

            $note = Config::get('commonConfig.pr_pdf_note');
        }


        $pr_number->pr_items=$pr_items??collect([]);

        if($pr_number->gst_no != null){
            $gst_rate = $pr_number->gst_no->gst_rate/100; 
        }else{
            $gst_rate = 0; 
        }
        
        if(!Session::has('code')){
            Session::put('approved_users', $pr_number->approved_user->email);
            Session::put('approved_users_name', $pr_number->approved_user->name);
        }
        $pr_number->tnc = explode("\r\n", $pr_number->tnc);
        
        $pdf = \PDF::loadView('myPDF', compact('pr_number', 'ids', 'title', 'gst_rate', 'note'));

        $pdf->setPaper('A4', 'potrait');
        
        return $pdf;

        // return $pdf->stream('public/pr/'.Auth()->user()->id.'_'.$id.'.pdf');
    }

    /**
     * Send mail
     */

    public function send_email($id, $pr_no, Request $request){
        if($request->code){
            Session::put('code_request', $request->code);
        }

        $pdf = $this->savePDF($id);
        
        if(array_key_exists('msg', $pdf)){
            return $pdf;
        }
        
        $ids = explode('_',$id);
        
        $this->mail($pdf, $id, $pr_no);
        
        if($ids[1] == 'pr'){
            return redirect()->route('pr_number.index');
        }
            
        if($ids[1] == 'po'){
            return redirect()->route('po_number.index');
        }        
    }


    public function mail($pdf, $id, $pr_no)
    {
        $content = $pdf->download()->getOriginalContent();
        $ids = explode('_',$id);

        if($ids[1] == 'po'){
            Storage::put('public/po/'.Auth()->user()->id.'_'.$id.'.pdf', $content);
            $path = 'public/po/'.Auth()->user()->id.'_'.$id.'.pdf';
            $path = Storage::disk('local')->path($path);
            
            $data = [];
            $data['path'] = $path;
            $data['name'] = 'PO raised by '.Auth()->user()->name.' for PR number '. $pr_no;
            $data['purchase'] = 'Purchase Order for PR number - '. $pr_no;
            $data['email'] = Config::get('commonConfig.email_id');
            $data['pr_no'] = $pr_no;
            
            if(Session::has('to_email')){
                $data['to_email'] = Session::get('to_email');
                Session::forget('to_email');
            }
        }

        if($ids[1] == 'pr'){
            Storage::put('public/pr/'.Auth()->user()->id.'_'.$id.'.pdf', $content);
            $path = 'public/pr/'.Auth()->user()->id.'_'.$id.'.pdf';
            $path = Storage::disk('local')->path($path);
            // dd($path);
            $data = [];
            $data['path'] = $path;
            $data['name'] = 'PR raised by '.Auth()->user()->name.", PR number - ".$pr_no;
            $data['approval'] = 'Please approve, '.Session::get('approved_users_name').'.';
            $data['purchase'] = 'Purchase Request - '. $pr_no;
            $data['email'] = Auth()->user()->email;
            $data['pr_no'] = $pr_no;
            $data['to_email'] = Config::get('commonConfig.email_id');
        }

        if(Session::has('approved_users')){
            $data['cc_email'] = Session::get('approved_users');
            Session::forget('approved_users');
            
            try {
                Mail::to($data['to_email'])->send(new SendMailable($data, $path));
            }
            catch (exception $e) {
                dd('msg:  ' .$e);
            }

        }else{
            $data['code'] = Session::get('code');
            $arr['approval_code'] = $data['code'];
            $po_number = PoNumber::where('id', $ids[0])->update($arr);
            Session::forget('code');
            
            try {
                Mail::to($data['to_email'])->send(new POApproval($data, $path));
            }
            catch (exception $e) {
                dd('msg:  ' .$e);
            }
        }
        

        return 'Email was sent';
    }

    function number_to_word($number = ''){
        $no = round($number);
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
        0 => '',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Forty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
        } else {
        $str [] = null;
        }
        }
        
        $Rupees = implode(' ', array_reverse($str));
        $paise = ($decimal) ? "And " . ($words[$decimal - $decimal%10]) ." " .($words[$decimal%10]) . " Paise " : '';
        return ($Rupees ? $Rupees .' Rupees ' : '') . $paise . " Only";
        }
}
