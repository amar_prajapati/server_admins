<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServerDetails;
use Auth;
use Config;
use App\Mail\SendServerDetails;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;
use App\Services\CustomRouteService as CustomRouteServices;

class ServerController extends Controller
{

    public function __construct(CustomRouteServices $customRouteServices)
    {
        $this->customRouteServices=$customRouteServices;
        $this->CommonController = new CommonController();
        $this->list_num_of_records_per_page = Config::get('commonConfig.list_num_of_records_per_page');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTables $datatables, Request $request)
    {
        $columns = [
            ['data' => 'rownum','name' => 'rownum','title' => 'Sr No.','searchable' => false],
            ['data' => 'email_id','name' => 'email_id','title' => 'Email', 'searchable' => true],
            ['data' => 'ip_addr','name' => 'ip_addr','title' => 'IP Address', 'searchable' => true],
            ['data' => 'actions','name' => 'actions','title' => 'Actions'],
        ];
        $getRequest = $request->all();
        $server_details = ServerDetails::orderBy('id', 'desc')->get();
        
        if ($datatables->getRequest()->ajax()) {

            return $datatables->of($server_details)
                ->setRowId(function ($server_details){
                    return 'row_'.$server_details->id;
                })
                ->editColumn('rownum', function ($server_details) {
                    static $i = 0;
                    $i++;
                    return $i;
                })
                ->editColumn('email_id', function ($server_details) {
                    return $server_details->email_id;
                })
                ->editColumn('ip_addr', function ($server_details) {
                    return $server_details->ip_addr;
                })
                ->editColumn('actions', function ($server_details) {
                    return view('admin.servers.actions', compact('server_details'));
                })
                ->rawColumns(['rownum', 'email_id', 'ip_addr', 'actions'])
                ->make(true);

        }

        $html = $datatables->getHtmlBuilder()->columns($columns)->parameters($this->getParameters());
        // dd($html);
        return view('admin.servers.index', compact('html'));
    }

    protected function getParameters() {
        return [
            'serverSide' => true,
            'processing' => true,
            'ordering'   =>'isSorted',
//            "order"=> [2, "desc" ],
            "pageLength" => $this->list_num_of_records_per_page,
            // 'fixedHeader' => [
            //     'header' => true,
            //     'footer' => true
            // ]
            "filter" => [
                'class' => 'test_class'
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validated_fields = ServerDetails::validate($request);
        if($validated_fields->fails()){
            $errors = $validated_fields->errors();
            $request->flash();
            if($request->email != null){
                return $errors;
            }
            else{
                return redirect()->route('server.create')->withErrors($errors)->withInput();
            }
        }else{
            $server_details_entered = ServerDetails::where([['email_id', '=', $request->email_id], ['ip_addr', '=', $request->ip_addr]])->first();
            
            if($server_details_entered != null){
                $request->flash();
                return redirect()->route('server.create')->with('error', 'Email and IP address already exist!');
            }else{
                $input = $request->all();
                unset($input['_token']);
                $input['password'] = encrypt($request->password);
                // dd($input);
                $server_details = ServerDetails::create($input);
                

                // if(isset($server_details)){

                //     $data = array(
                //         'to_email' => $input['email_id'],
                //         'from_email' => Auth::user()->email,
                //         'ip_addr' => $input['ip_addr'],
                //         'subject' => 'Server Details',
                //         'url' => route('generate.password', $server_details->id)
                //     );
                    
                //     // try {
                //     //     Mail::to($data['to_email'])->send(new SendServerDetails($data));
                //     // }
                //     // catch (exception $e) {
                //     //     dd('msg:  ' .$e);
                //     // }

                // }
                
                return redirect()->route('server.index')->with('success', 'The server details added successfully!');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $server_details = ServerDetails::where('id', $id)->first();
        
        return view('admin.servers.edit', compact('server_details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server_details_entered = ServerDetails::where([['email_id', '=', $request->email_id], ['ip_addr', '=', $request->ip_addr]])->first();
            
        // if($server_details_entered != null){
        //     $request->flash();
        //     return redirect()->route('server.edit', $server_details_entered->id)->with('error', 'Email and IP address already exist!');
        // }else{
            $input = $request->all();
            unset($input['_token'], $input['_method'], $input['conf_password']);
            if($request->password == null){
                $input['password'] = $server_details_entered->password;
            }else{
                $input['password'] = encrypt($request->password);
            }
            $server_details_updated = ServerDetails::where('id', $id)->update($input);
            
            if($server_details_updated == 1){
                return redirect()->route('server.index')->with('success', 'The server details updated successfully!');
            }
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
