<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServerDetails;
use App\Mail\SendOTP;
use Illuminate\Support\Facades\Mail;

class GeneratePasswordController extends Controller
{
    public function generate_password(){
        // dd($id);
        return view('generate_password');
    }

    public function check_server_creds($email_id, $ip_addr){
        $server_details = ServerDetails::where([['email_id', '=', $email_id], ['ip_addr', '=', $ip_addr]])->first();
        
        if($server_details != null){
            $data['server_details'] = $server_details;
            $data['success'] = 1;
            $update_arr['otp'] = rand(10, 99).$server_details->id.date('m');
            $data['subject'] = 'OTP';
            $data['from_email'] = 'amar.prajapati@neosofttech.com';
            $data['otp'] = $update_arr['otp'];
            $data['to_email'] = $email_id;
            try {
                Mail::to($data['to_email'])->send(new SendOTP($data));
            }
            catch (exception $e) {
                dd('msg:  ' .$e);
            }
            $update_server_details = ServerDetails::where('id', $server_details->id)->update($update_arr);
            return $data;
        }else{
            $data['server_details'] = $server_details;
            $data['success'] = 0;
            return $data;
        }

    }

    public function check_otp($otp, $id){
        $otp_details = ServerDetails::where([['otp', '=', $otp], ['id', '=', $id]])->first();
        if($otp_details != null){
            // dd(decrypt($otp_details->password));
            $update_arr['otp'] = 1;
            // $user_password = explode('@', $otp_details->email_id)[0];
            // $update_arr['password'] = encrypt($user_password);
            $server_details_updated = ServerDetails::where('id', $id)->update($update_arr);
            $server_details['password'] = decrypt($otp_details->password);
            $server_details['success'] = 1;
        }else{
            $server_details['success'] = 0;
        }
        return $server_details;
    }
}
