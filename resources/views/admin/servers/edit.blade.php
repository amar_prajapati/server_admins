@extends('layouts.app')
@section('body')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                Edit Server Details
            </h3>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Main Portlet-->
    <div class="m-portlet">
        {{--<div class="row">--}}
        @if (session('error'))
            <span class="text-danger">{{ session('error') }}</span>
        @endif
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="edit_server_details" action="{{ route('server.update', [$server_details->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-md-6">
                            <label>
                                Customer Email id:
                            </label>
                            <input type="text" class="form-control m-input" name="email_id" placeholder="Enter Email id" value="{{ $server_details->email_id }}">
                            <span class="text-danger">{{$errors->first('email_id')}}</span>
                        </div>
                        <div class="col-md-6">
                            <label>
                                IP address:
                            </label>
                            <input type="text" class="form-control m-input" name="ip_addr" placeholder="Enter IP address" value="{{ $server_details->ip_addr }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-md-6">
                            <label>
                                Passowrd:
                            </label>
                            <input type="password" class="form-control m-input" name="password" id="password" placeholder="Enter Password">
                        </div>
                        <div class="col-md-6">
                            <label>
                                Confirm Passowrd:
                            </label>
                            <input type="password" class="form-control m-input" name="conf_password" id="conf_password" placeholder="Confirm Password">
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                                <a href="{{ route('server.index') }}" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        {{--</div>--}}
    </div>
    <!--End::Main Portlet-->
</div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#edit_server_details').validate({
                rules: {
                    email_id:{
                        required: true,
                    },
                    ip_addr:{
                        required: true
                    },
                    // password:{
                    //     required: false
                    // },
                    conf_password: {
                        equalTo: "#password"
                    }
                },
                messages: {
                    email_id:{
                        required: "Please enter email id.",
                    },
                    ip_addr:{
                        required: "Please enter ip address."
                    },
                    // password:{
                    //     required: "Please enter password."
                    // },
                    conf_password: {
                        equalTo: "Password and confirm password must be same."
                    }
                }
            });
        });
    </script>
@endsection