<!--<!DOCTYPE html> -->
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 5.0.3
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->

<html lang="en" >
<!-- begin::Head -->
<head>
	<meta charset="utf-8" />
	<title>
		Server Admin
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="description" content="Latest updates and statistic charts">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
	</script>
	<!--end::Web font -->
	<!--begin::Base Styles -->
	<!--begin::Page Vendors -->
	<link href="{{ asset('/metronic/theme/dist/html/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors -->
	<link href="{{ asset('/metronic/theme/dist/html/default/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />

	<link href="{{asset('/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />


    <link href="{{ asset('/metronic/theme/dist/html/default/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->

    {{-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> --}}

	<link rel="shortcut icon" href="{{ asset('/metronic/theme/dist/html/default/assets/demo/default/media/img/logo/favicon.ico') }}" />
	<style>
		.has-danger label.col-form-label, .has-danger label.form-control-label, .has-danger label:not(.m-checkbox):not(.m-radio){
			color: inherit;
		}

		.has-success label.col-form-label, .has-success label.form-control-label, .has-success label:not(.m-checkbox):not(.m-radio){
			color: inherit;
		}

		#dor-error{
			position: absolute;
			top: 37px;
		}
	</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
	<!-- BEGIN: Header -->
	<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
		<div class="m-container m-container--fluid m-container--full-height">
			<div class="m-stack m-stack--ver m-stack--desktop">
				<!-- BEGIN: Brand -->
				<div class="m-stack__item m-brand  m-brand--skin-dark ">
					<div class="m-stack m-stack--ver m-stack--general">
						<div class="m-stack__item m-stack__item--middle m-brand__logo">
						<span style="color:white"><img width="150" height="55" src="{{ asset('images/web-werks-data-logo.png') }}"></span>
							<a href="index.html" class="m-brand__logo-wrapper">
								<img alt="" src="assets/demo/default/media/img/logo/logo_default_dark.png"/>
							</a>
						</div>
						<div class="m-stack__item m-stack__item--middle m-brand__tools">
							<!-- BEGIN: Left Aside Minimize Toggle -->
							<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
					 ">
								<span></span>
							</a>
							<!-- END -->
							<!-- BEGIN: Responsive Aside Left Menu Toggler -->
							<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a>
							<!-- END -->
							<!-- BEGIN: Responsive Header Menu Toggler -->
							<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a>
							<!-- END -->
							<!-- BEGIN: Topbar Toggler -->
							<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
								<i class="flaticon-more"></i>
							</a>
							<!-- BEGIN: Topbar Toggler -->
						</div>
					</div>
				</div>
				<!-- END: Brand -->
				<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
					<!-- BEGIN: Topbar -->
					<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
						<div class="m-stack__item m-topbar__nav-wrapper">
							<ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
									
									<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="{{ asset('metronic/theme/src/media/app/img/users/user-6.jpg') }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
										<span class="m-topbar__username m--hide">
													Nick
												</span>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav m-nav--skin-light">
														<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
														</li>
														<li class="m-nav__item">
															<i class="m-nav__link-icon flaticon-user-1"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">
																		{{ Auth::user()->name }}
																	</span>
																</span>
															</span>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="{{ url('logout') }}" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																Logout
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li id="m_quick_sidebar_toggle" class="m-nav__item">
                                    <a href="{{ url('logout') }}" class="m-nav__link m-dropdown__toggle">
                                    </a>
								</li>
							</ul>
						</div>
					</div>
					<!-- END: Topbar -->
				</div>
			</div>
		</div>
	</header>
	<!-- END: Header -->
	<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
			<i class="la la-close"></i>
		</button>
		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
			<!-- BEGIN: Aside Menu -->
			<div
					id="m_ver_menu"
					class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
					data-menu-vertical="true"
					data-menu-scrollable="false" data-menu-dropdown-timeout="500"
			>
				@include('layouts.sidebar')
			</div>
			<!-- END: Aside Menu -->
		</div>
		<!-- END: Left Aside -->
		<div class="m-grid__item m-grid__item--fluid m-wrapper">

                @if(session()->has('message'))
                @php
                    $message=session()->get('message')
                @endphp
				<br/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-dismissible alert-{{$message['type']}}" id="success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                    <strong>{{ucfirst($message['type'])}}!! </strong>
                                    {{$message['text']}}
                            </div>
                        </div>
                    </div>
                </div>
                @endif
			@section('body')
				@show
		</div>
	</div>
	<!-- end:: Body -->
	<!-- begin::Footer -->
	
	<!-- end::Footer -->
</div>
<!-- end:: Page -->
<!-- begin::Quick Sidebar -->
<!-- end::Quick Sidebar -->
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
<!-- begin::Quick Nav -->
<!-- begin::Quick Nav -->
<!--begin::Base Scripts -->
<script src="{{ asset('/metronic/theme/dist/html/default/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('/metronic/theme/dist/html/default/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
<script src="{{ asset('/metronic/theme/dist/html/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('/metronic/theme/dist/html/default/assets/app/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{asset('/plugins/datatables/datatables.all.min.js')}}" type="text/javascript"></script>

<script src="{{ asset('/metronic/theme/dist/html/default/assets/demo/default/custom/components/forms/validation/form-widgets.js') }}" type="text/javascript"></script>
<script src="{{ asset('/metronic/theme/dist/html/default/assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- <script src="{{asset('/metronic/docs/assets/plugins/jquery/dist/jquery.min.js')}}" type="text/javascript"></script> -->
@yield('js')

@yield('datatablejs')
<!--end::Page Snippets -->
</body>
<!-- end::Body -->



<style>
.pagination li {
    padding: 0;
    border-radius: 50%;
    background: #f2f3f8;
    margin: 5px;
    height: 2.25rem;
    width: 2.25rem;
    position: relative;
    align-items: center;
    text-align: center;
    vertical-align: middle;
    font-size: 1rem;
    line-height: 1rem;
    font-weight: 400;
    justify-content: center;
    display: flex;
}
.pagination li.active{
    background: #716aca;
        color:white !important;
}
.pagination li.active a{
    color:white !important;
}

.pagination li.disabled,.pagination li.disabled a{
    cursor:none !important;
}

</style>
</html>
