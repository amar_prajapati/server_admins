@extends('layouts.layout')
@section('body')
  <!-- <img id="bg_img" src="dc_2.jpg" width="100%" height="100%"> -->
<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark scrolling-navbar bg-white fixed-top">
    <img width="145" height="52" src="{{ asset('images/web-werks-data-logo.png') }}">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
      aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
      <ul class="navbar-nav ml-auto">
      </ul>
    </div>
  </nav>
  <!--/.Navbar -->
<div class="container" id="form_container">
  <!-- <div class="col-md-12"> -->
    <!-- <center> -->
      <div class="col-md-5" style="margin-left: 30%;">
        <h4><b><span id="form_title">Generate password</span></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" id="back_btn" href="javascript:void();" style="display:none;">Back</a></h4>
        <div class="alert alert-success" style="text-align: center; display: none" id="success">
        </div>
        <div class="alert alert-danger" style="text-align: center; display: none" id="failure">
        </div>
        <form id="conf_form">
          <div id="hide_grp">
            <div class="md-form" id="email_grp">
              <i class="fas fa-envelope prefix"></i>
              <input type="email" class="form-control" id="email" name="email" autocomplete="off">
              <label for="email">Email:</label>
              <!-- <p id="err_email" style="color:red"></p> -->
            </div>
            <div class="md-form" id="ip_address_grp">
              <i class="fas fa-server prefix"></i>
              <input type="text" class="form-control" id="ip_address" name="ip_address" autocomplete="off">
              <label for="ip_address">IP Address:</label>
              <!-- <p id="err_ip_addr" style="color:red"></p> -->
            </div>
          </div>
          <div class="md-form" id="otp_grp">
            <i class="fas fa-key prefix"></i>
            <label for="otp">OTP:</label>
            <input type="text" class="form-control" id="otp" name="otp">
            <!-- <label for="otp" data-error="wrong" data-success="right">adasdadads</label> -->
            <!-- <p id="err_otp" style="color:red"></p> -->
          </div>
          <center>
            <input type="submit" class="btn btn-primary" id="form_submit" onclick="onSubmit();" value="Submit">
          </center>
        </form>
      </div><br/>
    <div class="table-responsive-md">
        <table id="details" class="table table-bordered" style="display:none">
            <thead class="thead-dark">
              <tr>
                    <th scope="row"><b>Email:</b></th>
                    <th scope="row"><b>IP Address:</b></th>
                    <th scope="row"><b>Password:</b></th>
              </tr>
            </thead>
            <tbody>
                <tr>
                    <td><span id="email_span"></span></td>
                    <td><span id="ip_addr_span"></span></td>
                    <td><span id="passwd_span"></span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- </center> -->
  <!-- </div> -->
</div>
@endsection
@section('script')
<script>

    function close_modal(){
        location.reload(); 
    }

    $('#back_btn').on('click', function(){
        location.reload();
    });
    
    function onSubmit(){
      var email = $('input[name=email]').val();
      var ip_addr = $('input[name=ip_address]').val();

      $.validator.addMethod(
        "email_validate",function(){       
          if(email != "" && ip_addr != ""){
            $('input[name=email]').css({"color": "black"});
            sessionStorage.setItem("is_otp", "1");
            sessionStorage.setItem("email", email);
            sessionStorage.setItem("ip_addr", ip_addr);
            location.reload();
          }
        },""
      );      

      $.validator.addMethod(
        "otp_validate",function(){
          var otp = $('input[name=otp]').val();
          var email = sessionStorage.getItem("email");
          var ip_addr = sessionStorage.getItem("ip_addr");

          if(otp.length >= 5){
            var passwd = email.split("@")[0];
            sessionStorage.setItem("password", passwd);
            sessionStorage.setItem("is_otp_enter", "1");
            sessionStorage.setItem("otp", otp);
            location.reload();
          }else{
            $('#failure').show().text('OTP is invalid.').delay(600).slideUp(600);
          }
        },""
      );
      
      $('#conf_form').validate({
        rules:{
          email:{
            required:true,
            email:true,
            email_validate: true
          },
          ip_address:{
            required:true,
          },
          otp:{
            required:true,
            number:true,
            otp_validate: true
          }
        },
        messages:{
          email:{
            required:"Enter the email id!",
            email:"Enter the valid email id!"
          },
          ip_address:{
            required:"Enter the ip address!"
          },
          otp:{
            required:"Enter OTP",
            number:"Enter the valid OTP"
          }
        },
        // errorPlacement: function() {
        //   $('#email-error').css({"margin-top": "53px", "color": "red", "font-size": "large", "font-weight": "400"});
        //   $('#ip_address-error').css({"margin-top": "53px", "color": "red", "font-size": "large", "font-weight": "400"});
        //   $('#otp-error').css({"margin-top": "53px", "color": "red", "font-size": "large", "font-weight": "400"});
        // },
        submitHandler: function (form) {
            // $('label .error').css({'margin-top': '7%'});
            // console.log($('label .error').length);
        }
      });

      $('.error').css({'margin-top': '7%'});
    }

    if(sessionStorage.getItem("is_otp") == 1){
      var email_id = sessionStorage.getItem("email");
      var ip_addr = sessionStorage.getItem("ip_addr");
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
          type:'GET',
          url:'/check_server_creds/' + encodeURI(email_id) + '/' + encodeURI(ip_addr),
        //  data:{ 
        //       'email_id': email,
        //       'ip_addr': ip_addr
        //   },
          success:function(data) {
            if(data.success == 1){
              sessionStorage.setItem('id', data.server_details.id);
              $('#hide_grp').hide();
              $('#otp_grp').show();
              $('#form_title').text('Enter OTP');
              sessionStorage.removeItem("is_otp");
            }else{
              $('#failure').show().text('Email id or IP address is invalid.').delay(600).slideUp(600);
              sessionStorage.removeItem("email");
              sessionStorage.removeItem("ip_addr");
              sessionStorage.removeItem("is_otp");
            }
          }
      });
    }

    if(sessionStorage.getItem("is_otp_enter") == 1){
      var otp = sessionStorage.getItem('otp');
      var id = sessionStorage.getItem('id');
      $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type:'GET',
            url:'/check_otp/' + encodeURI(otp) + '/' + encodeURI(id),
          //  data:{ 
          //       'email_id': email,
          //       'ip_addr': ip_addr
          //   },
            success:function(data) {
              console.log(data);
              if(data.success == 1){
                var email = sessionStorage.getItem("email");
                var ip_addr = sessionStorage.getItem("ip_addr");
                var passwd = data.password;
                $('#hide_grp').hide();
                $('#form_submit').hide();
                $('#form_title').text('Server Details');
                $('#details').show();
                $('#email_span').text(email);
                $('#ip_addr_span').text(ip_addr);
                $('#passwd_span').text(passwd);
                $('#back_btn').show();
                sessionStorage.removeItem("email");
                sessionStorage.removeItem("ip_addr");
                sessionStorage.removeItem("is_otp_enter");
                sessionStorage.removeItem("otp");
                sessionStorage.removeItem("password");
                sessionStorage.removeItem("id");
              }else{
                if(sessionStorage.getItem('is_otp_enter') == 1){
                  $('#failure').show().text('OTP is invalid.').delay(600).slideUp(600);
                  $('#hide_grp').hide();
                  $('#otp_grp').show();
                  $('#form_title').text('Enter OTP');
                }
                // location.reload();
                // sessionStorage.removeItem("email");
                // sessionStorage.removeItem("ip_addr");
                // sessionStorage.removeItem("is_otp");
              }
            }
        });  
    }

    $('#email').on('focusin', function(){
        if($('#email-error').length == 1){
          $('#email-error').css({"margin-top":"12%", "font-size": "20px"});
        }
    });

    $('#email').on('focusout', function(){
      if($('#email-error').length == 1){
          $('#email-error').css({"margin-top":"7%", "font-size": "16px"});
      }
    });

    $('#ip_address').on('focusin', function(){
        if($('#ip_address-error').length == 1){
          $('#ip_address-error').css({"margin-top":"12%", "font-size": "20px"});
        }
    });

    $('#ip_address').on('focusout', function(){
      if($('#ip_address-error').length == 1){
          $('#ip_address-error').css({"margin-top":"7%", "font-size": "16px"});
      }
    });

    $('#otp').on('focusin', function(){
        if($('#otp-error').length == 1){
          $('#otp-error').css({"margin-top":"12%", "font-size": "20px"});
        }
    });

    $('#otp').on('focusout', function(){
      if($('#otp-error').length == 1){
          $('#otp-error').css({"margin-top":"7%", "font-size": "16px"});
      }
    });
</script>
@endsection
